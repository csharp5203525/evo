﻿using Newtonsoft.Json;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System;
using System.Reactive;

namespace EvoTestTask
{
    public class ContactViewModel : ReactiveObject
    {
        [JsonProperty][Reactive] public Guid Id { get; set; }
        [JsonProperty][Reactive] public string FirstName { get; set; }
        [JsonProperty][Reactive] public string LastName { get; set; }
        [JsonProperty][Reactive] public string Phone { get; set; }
        [JsonProperty][Reactive] public string Mail { get; set; }

        public ContactViewModel()
        {
        }

        public ReactiveCommand<Unit, Unit> OpenPage { get; }
    }
}
﻿using DynamicData;
using Newtonsoft.Json;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace EvoTestTask
{
    public class AppViewModel : ReactiveObject
    {
        [Reactive] public IEnumerable<ContactViewModel> Contacts { get; set; }
        [Reactive] public string FirstName { get; set; }
        [Reactive] public string LastName { get; set; }
        [Reactive] public string Phone { get; set; }
        [Reactive] public string Mail { get; set; }

        private readonly string _file = @"ContactList.json";

        [Reactive] public string SearchTerm { get; set; }

        private readonly ObservableAsPropertyHelper<IEnumerable<ContactViewModel>> _searchResults;
        public IEnumerable<ContactViewModel> SearchResults => _searchResults.Value;

        private readonly ObservableAsPropertyHelper<bool> _isAvailable;
        public bool IsSearchPanelVisible => _isAvailable.Value;
        private readonly ObservableAsPropertyHelper<bool> _isContactPanelVisible;
        public bool IsContactPanelVisible => _isContactPanelVisible.Value;

        public SourceList<ContactViewModel> sourceList;

        public AppViewModel()
        {
            Contacts = new List<ContactViewModel>();
            if (File.Exists(_file))
            {
                string text = File.ReadAllText(_file);
                var contacts = JsonConvert.DeserializeObject<IEnumerable<ContactViewModel>>(text);
                if (contacts != null)
                {
                    Contacts = contacts;
                }
            }
            else
            {
                File.WriteAllText(_file, JsonConvert.SerializeObject(Contacts));
            }

            _searchResults = this
                .WhenAnyValue(x => x.SearchTerm, a => a.Contacts)
                .Throttle(TimeSpan.FromMilliseconds(100))
                .Select(term => term.Item1?.Trim())
                .DistinctUntilChanged()
                .SelectMany(SearchContacts)
                .ObserveOn(RxApp.MainThreadScheduler)
                .ToProperty(this, x => x.SearchResults);

            _searchResults.ThrownExceptions.Subscribe(error => { /* Handle errors here */ });
            _isAvailable = this
                .WhenAnyValue(x => x.SearchResults)
                .Select(searchResults => searchResults != null || !string.IsNullOrEmpty(SearchTerm))
                .ToProperty(this, x => x.IsSearchPanelVisible);
        }

        private async Task<IEnumerable<ContactViewModel>> SearchContacts(string term, CancellationToken token)
        {
            if (string.IsNullOrEmpty(term))
            {
                return Contacts;
            }
            var result = Contacts.SelectMany(x => Contacts.Where(eo => eo.FirstName.Contains(term)));
            return result;
        }

        public void AddNewContact()
        {
            if (Mail.Contains("spam"))
            {
                MessageBox.Show("Адрес почты не должен содержать \"spam\"", "Ошибка");
                return;
            }

            if (string.IsNullOrWhiteSpace(FirstName) ||
                string.IsNullOrWhiteSpace(LastName) ||
                string.IsNullOrWhiteSpace(Phone) ||
                string.IsNullOrWhiteSpace(Mail))
            {
                MessageBox.Show("Не все поля заполнены", "Ошибка");
                return;
            }

            var contactList = Contacts.ToList();
            var contact = new ContactViewModel();
            contact.FirstName = FirstName;
            contact.LastName = LastName;
            contact.Phone = Phone;
            contact.Mail = Mail;
            contactList.Add(contact);

            Contacts = contactList;

            FirstName = string.Empty;
            LastName = string.Empty;
            Phone = string.Empty;
            Mail = string.Empty;
        }

        public void SaveContacts()
        {
            File.WriteAllText(_file, JsonConvert.SerializeObject(Contacts));
        }
    }
}
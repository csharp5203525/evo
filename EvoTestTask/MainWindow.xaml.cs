﻿using ReactiveUI;
using System.Linq;
using System.Reactive.Disposables;
using System.Windows;

//using System.Windows.Forms;
//using System.Windows.Forms;

namespace EvoTestTask
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();

            ViewModel = new AppViewModel();

            this.WhenActivated(disposableRegistration =>
            {
                this.OneWayBind(ViewModel,
                    viewModel => viewModel.Contacts,
                    view => view.ContactList.ItemsSource)
                    .DisposeWith(disposableRegistration);

                //this.OneWayBind(ViewModel,
                //    viewModel => viewModel.SearchResults,
                //    view => view.SearchResults.ItemsSource)
                //    .DisposeWith(disposableRegistration);

                //this.OneWayBind(ViewModel,
                //    viewModel => viewModel.IsSearchPanelVisible,
                //    view => view.SearchResults.Visibility)
                //    .DisposeWith(disposableRegistration);

                this.Bind(ViewModel, viewModel => viewModel.FirstName, view => view.FirstName.Text).DisposeWith(disposableRegistration);
                this.Bind(ViewModel, viewModel => viewModel.LastName, view => view.LastName.Text).DisposeWith(disposableRegistration);
                this.Bind(ViewModel, viewModel => viewModel.Phone, view => view.Phone.Text).DisposeWith(disposableRegistration);
                this.Bind(ViewModel, viewModel => viewModel.Mail, view => view.Mail.Text).DisposeWith(disposableRegistration);

                this.Bind(ViewModel, viewModel => viewModel.FirstName, view => view.LabelFirstName.Content).DisposeWith(disposableRegistration);
                this.Bind(ViewModel, viewModel => viewModel.LastName, view => view.LabelLastName.Content).DisposeWith(disposableRegistration);
                this.Bind(ViewModel, viewModel => viewModel.Phone, view => view.LabelPhone.Content).DisposeWith(disposableRegistration);
                this.Bind(ViewModel, viewModel => viewModel.Mail, view => view.LabelMail.Content).DisposeWith(disposableRegistration);

                this.Bind(ViewModel, viewModel => viewModel.SearchTerm, view => view.SearchTerm.Text).DisposeWith(disposableRegistration);
            });

            ContactList.SelectionChanged += SelectorContact;
        }

        private void SortByName(object sender, RoutedEventArgs e)
        {
            var contacts = ViewModel.Contacts.ToList();
            ViewModel.Contacts = contacts.OrderBy(o => o.FirstName).ToList();
        }

        private void SortByLastName(object sender, RoutedEventArgs e)
        {
            var contacts = ViewModel.Contacts.ToList();
            ViewModel.Contacts = contacts.OrderBy(o => o.LastName).ToList();
        }

        private void SortByPhone(object sender, RoutedEventArgs e)
        {
            var contacts = ViewModel.Contacts.ToList();
            ViewModel.Contacts = contacts.OrderBy(o => o.Phone).ToList();
        }

        private void SortByMail(object sender, RoutedEventArgs e)
        {
            var contacts = ViewModel.Contacts.ToList();
            ViewModel.Contacts = contacts.OrderBy(o => o.Mail).ToList();
        }

        private void AddNewContact(object sender, RoutedEventArgs e)
        {
            ShowEditPanel(true);
            ButtonCompleteAddContact.Visibility = Visibility.Visible;
            ButtonCompleteEditContact.Visibility = Visibility.Collapsed;
        }

        private void EditContact(object sender, RoutedEventArgs e)
        {
            ShowEditPanel(true);
            ButtonCompleteAddContact.Visibility = Visibility.Collapsed;
            ButtonCompleteEditContact.Visibility = Visibility.Visible;
        }

        private void RemoveContact(object sender, RoutedEventArgs e)
        {
            ContactViewModel contact = (ContactViewModel)ContactList.SelectedItem;
            var contacts = ViewModel.Contacts.ToList();
            contacts.Remove(contact);
            ViewModel.Contacts = contacts;
        }

        private void CompleteEditContact(object sender, RoutedEventArgs e)
        {
            ContactViewModel contact = (ContactViewModel)ContactList.SelectedItem;
            if (this.Mail.Text.Contains("spam"))
            {
                System.Windows.MessageBox.Show("Адрес почты не должен содержать \"spam\"", "Ошибка");
                return;
            }

            if (string.IsNullOrWhiteSpace(this.FirstName.Text) ||
                string.IsNullOrWhiteSpace(this.LastName.Text) ||
                string.IsNullOrWhiteSpace(this.Phone.Text) ||
                string.IsNullOrWhiteSpace(this.Mail.Text))
            {
                System.Windows.MessageBox.Show("Не все поля заполнены", "Ошибка");
                return;
            }

            contact.FirstName = this.FirstName.Text;
            contact.LastName = this.LastName.Text;
            contact.Phone = this.Phone.Text;
            contact.Mail = this.Mail.Text;

            ShowEditPanel(false);
        }

        private void CompleteAddContact(object sender, RoutedEventArgs e)
        {
            ViewModel.AddNewContact();
            ShowEditPanel(false);
        }

        private void CancelAddContact(object sender, RoutedEventArgs e)
        {
            ShowEditPanel(false);
        }

        private void SelectorContact(object sender, RoutedEventArgs e)
        {
            ContactViewModel contact = (ContactViewModel)ContactList.SelectedItem;
            if (contact == null)
            {
                return;
            }
            ViewModel.FirstName = contact.FirstName;
            ViewModel.LastName = contact.LastName;
            ViewModel.Phone = contact.Phone;
            ViewModel.Mail = contact.Mail;
        }

        private void ShowEditPanel(bool show)
        {
            if (show)
            {
                this.LabelFirstName.Visibility = Visibility.Collapsed;
                this.LabelLastName.Visibility = Visibility.Collapsed;
                this.LabelPhone.Visibility = Visibility.Collapsed;
                this.LabelMail.Visibility = Visibility.Collapsed;

                this.FirstName.Visibility = Visibility.Visible;
                this.LastName.Visibility = Visibility.Visible;
                this.Phone.Visibility = Visibility.Visible;
                this.Mail.Visibility = Visibility.Visible;

                EditPanel.Visibility = Visibility.Visible;
            }
            else
            {
                this.LabelFirstName.Visibility = Visibility.Visible;
                this.LabelLastName.Visibility = Visibility.Visible;
                this.LabelPhone.Visibility = Visibility.Visible;
                this.LabelMail.Visibility = Visibility.Visible;

                this.FirstName.Visibility = Visibility.Collapsed;
                this.LastName.Visibility = Visibility.Collapsed;
                this.Phone.Visibility = Visibility.Collapsed;
                this.Mail.Visibility = Visibility.Collapsed;

                EditPanel.Visibility = Visibility.Collapsed;
            }
        }

        private void OnExit(object sender, System.ComponentModel.CancelEventArgs e)
        {
            ViewModel.SaveContacts();
        }
    }
}